# Crackbox UI
An Angular project using the [Crackbox API](../crackbox-api) to interface with a Hashtopolis installation.

You can find more details on the design decisions made in this project on the [Crackbox UI wiki page](https://gitlab.com/prexus/crackbox/-/wikis/crackbox-ui).


## Installation
Using `yarn` is required as Storybooks seems to have some issues with `npm` in the current version.

```bash
$ yarn install
```

## Running the app

```bash
$ yarn start
```

After starting the application the endpoint is available on `http://localhost:4200`.

Do not forget to also run the [Crackbox API](../crackbox-api) and point it to either a running Hashtopolis installation or alternatively use the provided [Hashtopolis Mock API](../hashtopolis-mock-api).

## Docker bundle
Gitlab will build a docker container of [Crackbox API](../crackbox-api) containing a static hosted version Crackbox UI. The container can be downloaded from `registry.gitlab.com/prexus/crackbox` and is available with both a `:latest` and a versioned tag.

For details on how to run the container, please refer to the [Crackbox API README.md](../crackbox-api/README.md).
