import { Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges } from '@angular/core';

import { Task } from '@cbox-api/interfaces/task.interface';
import { EventService } from '@cbox/services/event.service';

@Component({
    selector: 'cbox-task-list',
    templateUrl: './task-list.component.html',
    styleUrls: ['./task-list.component.scss']
})
export class TaskListComponent implements OnInit, OnChanges {
    @Input() loading: boolean = true;
    @Input() tasks: Task[] = [];

    @Output() selectTask = new EventEmitter<Task>();
    @Output() openCreateTask = new EventEmitter<void>();

    selectedTask: Task = null;

    constructor(private eventService: EventService) { }

    ngOnInit(): void {
        this.eventService.tasks$.subscribe((tasks) => {
            this.loading = false;
            this.tasks = tasks;

            if (tasks.length > 0) {
                this.onSelectTask(tasks[0]);
            }
        });
    }

    ngOnChanges(changes: SimpleChanges) {
        if (changes['tasks'] && this.tasks.length > 0) {
            this.selectedTask = this.tasks[0];
        }
    }

    taskTracker(_index: number, task: Task) {
        return task.id;
    }

    toggleCreateTasks() {
        this.openCreateTask.emit();
    }

    onSelectTask(task: Task) {
        this.selectedTask = task;
        this.selectTask.emit(task);
    }
}
