import { Component, EventEmitter, Input, Output } from '@angular/core';

import { Task } from '@cbox-api/interfaces/task.interface';

@Component({
    selector: 'cbox-task',
    templateUrl: './task.component.html',
    styleUrls: ['./task.component.scss']
})
export class TaskComponent {
    @Input() task!: Task;

    @Output() deleteTask = new EventEmitter<Task>();
    @Output() downloadTask = new EventEmitter<Task>();

    delete() {
        this.deleteTask.emit(this.task);
    }

    download() {
        this.downloadTask.emit(this.task);
    }
}
