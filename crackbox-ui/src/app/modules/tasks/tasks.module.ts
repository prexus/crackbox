import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';

import { EventService } from '@cbox/services/event.service';
import { TaskService } from '@cbox/services/task.service';

import { PipesModule } from '@cbox/pipes/pipes.module';

import { SharedComponentsModule } from '@cbox/components/shared-components.module';

import { CreateTaskComponent } from './create-task/create-task.component';
import { TaskComponent } from './task/task.component';
import { TaskListComponent } from './task-list/task-list.component';

import { TaskDetailsComponent } from './task-details/task-details.component';

@NgModule({
    declarations: [
        CreateTaskComponent,
        TaskComponent,
        TaskListComponent,
        TaskDetailsComponent
    ],
    providers: [
        EventService,
        TaskService
    ],
    imports: [
        BrowserModule,
        FormsModule,
        HttpClientModule,

        SharedComponentsModule,

        PipesModule
    ],
    exports: [
        CreateTaskComponent,
        TaskDetailsComponent,
        TaskListComponent
    ]
})
export class TasksModule { }
