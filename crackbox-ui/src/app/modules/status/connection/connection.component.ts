import { Component, Input } from '@angular/core';

@Component({
    selector: 'cbox-connection',
    templateUrl: './connection.component.html',
    styleUrls: ['./connection.component.scss']
})
export class ConnectionComponent {
    @Input() connected = false;
}
