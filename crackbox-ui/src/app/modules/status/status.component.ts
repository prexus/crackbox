import { Component, OnInit } from '@angular/core';

import { Agent } from '@cbox-api/interfaces/agent.interface';

import { EventService } from '@cbox/services/event.service';

@Component({
    selector: 'cbox-status',
    templateUrl: './status.component.html',
    styleUrls: ['./status.component.scss']
})
export class StatusComponent implements OnInit {
    agents: Agent[] = [];
    connected = false;

    constructor(private eventService: EventService) { }

    ngOnInit(): void {
        this.eventService.agents$.subscribe((agents) => {
            this.agents = agents;
        });

        this.eventService.connected$.subscribe((connected) => {
            this.connected = connected;
        });
    }

    agentTracker(_index: number, task: Agent) {
        return task.id;
    }
}
