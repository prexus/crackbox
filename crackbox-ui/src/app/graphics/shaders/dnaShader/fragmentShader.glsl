varying vec2 vUv;
varying vec3 vPosition;
varying vec3 vColor;

void main()	{
	float alpha = 1. - smoothstep(0.3, 0.5, length(gl_PointCoord - vec2(0.5)));
	alpha *= 0.5;

	float gradient = smoothstep(0.1, 0.05, vUv.y);

	gl_FragColor = vec4(vColor, alpha * gradient);
}
