import fragmentShader from './fragmentShader.glsl';
import vertexShader from './vertexShader.glsl';

export const dnaShader = {
    vertexShader,
    fragmentShader
}
