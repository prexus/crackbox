import fragmentShader from './fragmentShader.glsl';
import vertexShader from './vertexShader.glsl';

export const aberrationShader = {
    uniforms: {
        tDiffuse: { value: null },
        distort: { value: 0.5 }
    },
    vertexShader: vertexShader,
    fragmentShader: fragmentShader
}
