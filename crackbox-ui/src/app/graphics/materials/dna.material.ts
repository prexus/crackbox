import * as THREE from 'three';

import { dnaShader } from 'src/app/graphics/shaders/dnaShader/dnaShader';

const colors = {
    dna: [
        new THREE.Color(0x612574),
        new THREE.Color(0x293583),
        new THREE.Color(0x1954ec)
    ],
    pending: [
        new THREE.Color(0x553333),
        new THREE.Color(0x334440),
        new THREE.Color(0x223344)
    ],
    processed: [
        new THREE.Color(0x00ff00),
        new THREE.Color(0x11ff11),
        new THREE.Color(0x11ee00)
    ]
};

const material = new THREE.ShaderMaterial({
    side: THREE.DoubleSide,
    transparent: true,
    vertexShader: dnaShader.vertexShader,
    fragmentShader: dnaShader.fragmentShader,

    // Workaround to avoid artifacts with overlapping particles
    depthTest: false,
    depthWrite: false,

    blending: THREE.AdditiveBlending
});

export { material, colors };
