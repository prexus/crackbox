import * as THREE from 'three';
import { Points } from 'three';

import { material, colors } from '@cbox/materials/dna.material';

export interface DnaBasePairOptions {
    basePairWidth: number;
    basePairCount: number;
    basePairProgressRows: number;
    basePairHorizontalSpacing: number;
    basePairVerticalSpacing: number;
    basePairOffset: number;
}

export class Dna {
    private model: Points<THREE.BufferGeometry, THREE.ShaderMaterial>;
    private geometry = new THREE.BufferGeometry();
    private points: number;

    private colorMatrix: Float32Array;
    private colorIndexes: Record<number, number> = {}; // <point, index>

    constructor(private progress: number[], private options: DnaBasePairOptions) {
        this.points = options.basePairWidth * options.basePairCount;
        this.colorMatrix = new Float32Array(this.points * 3); // * 3 because we store R, G, B

        this.model = this.buildDna();
    }

    public getModel() {
        return this.model;
    }

    private buildDna() {
        const vertices = this.points * 3;

        const positions = new Float32Array(vertices);
        const size = new Float32Array(this.points);

        for (let point = 0; point < this.points; point++) {
            size.set([Math.random()], point);

            const basePairRowIndex = Math.floor(point / this.options.basePairWidth);
            const basePairPositionIndex = point % this.options.basePairWidth;

            const positionFromCenter = basePairPositionIndex - (this.options.basePairWidth / 2);
            const angleOffset = Math.abs(positionFromCenter) / this.options.basePairWidth;
            const angle = this.options.basePairOffset * Math.PI * 2 * basePairRowIndex;

            const theta = angle + angleOffset;

            const radius = this.options.basePairVerticalSpacing * (basePairPositionIndex - (this.options.basePairWidth / 2));

            const x = radius * Math.cos(theta);
            const y = this.options.basePairHorizontalSpacing * basePairRowIndex - ((this.options.basePairCount - 1) * (this.options.basePairHorizontalSpacing / 2));
            const z = radius * Math.sin(theta);

            positions.set([x, y, z], point * 3);

            const { index, color } = this.getColor('dna');

            this.colorIndexes[point] = index;
            this.colorMatrix.set(color.toArray(), point * 3);
        }

        this.rebuildActiveBasePairs(this.progress);

        this.geometry.setAttribute('position', new THREE.BufferAttribute(positions, 3));
        this.geometry.setAttribute('size', new THREE.BufferAttribute(size, 1));
        this.geometry.setAttribute('color', new THREE.BufferAttribute(this.colorMatrix, 3));

        return new THREE.Points(this.geometry, material);
    }

    rebuildActiveBasePairs(progress: number[]) {
        if (!progress) {
            return;
        }

        const activeBasePairs = this.options.basePairProgressRows;
        const totalBasePairs = this.options.basePairCount;
        const inactiveBasePairs = totalBasePairs - activeBasePairs;

        const pointsTotal = activeBasePairs * this.options.basePairWidth;
        const pointsStart = Math.floor(inactiveBasePairs / 2) * this.options.basePairWidth;

        for (let i = 0; i < pointsTotal; i++) {
            const activePoint = pointsStart + i;
            const markAsProcessed = progress.includes(i);

            const colorIndex = this.colorIndexes[activePoint];
            const { color } = this.getActiveBasePairColor(markAsProcessed, colorIndex);

            this.colorMatrix.set(color.toArray(), activePoint * 3);
        }

        this.geometry.setAttribute('color', new THREE.BufferAttribute(this.colorMatrix, 3));
    }

    public getColor(type: string, previousIndex?: number) {
        const candidates = colors[type];
        const randomIndex = Math.floor(Math.random() * candidates.length);
        const index = previousIndex || randomIndex;
        const color = candidates[index];

        return { color, index };
    }

    public getActiveBasePairColor(isPointProcessed: boolean, colorIndex: number) {
        return isPointProcessed ? this.getColor('processed', colorIndex) : this.getColor('pending', colorIndex);
    }
}
