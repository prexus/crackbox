import { Component, ElementRef, OnDestroy, OnInit } from '@angular/core';

@Component({
    selector: 'cbox-button',
    templateUrl: './button.component.html',
    styleUrls: ['./button.component.scss']
})
export class ButtonComponent implements OnInit, OnDestroy {
    height: number;
    width: number;

    strokeWidth: number;
    points: string;

    textPositionY: string;

    private resizeObserver: ResizeObserver;
    private container: HTMLElement;
    private polygon: SVGPolygonElement;
    private text: SVGTextElement;

    private onMouseEnter = () => { this.text.classList.add('hover'); };
    private onMouseLeave = () => { this.text.classList.remove('hover'); };

    constructor(private host: ElementRef) {}

    ngOnInit() {
        this.container = this.host.nativeElement;
        this.polygon = this.container.querySelector('polygon');
        this.text = this.container.querySelector('text');

        this.addEventListeners();
        this.setupResizeObserver();
    }

    ngOnDestroy(): void {
        this.removeEventListeners();
        this.disconnectResizeObserver();
    }

    addEventListeners() {
        this.polygon.addEventListener('mouseenter', this.onMouseEnter);
        this.polygon.addEventListener('mouseleave', this.onMouseLeave);
    }

    setupResizeObserver() {
        this.resizeObserver = new ResizeObserver(() => this.resize());
        this.resizeObserver.observe(this.container);

        this.resize();
    }

    resize() {
        const dimensions = this.getButtonDimensions();

        this.height = dimensions.height;
        this.width = dimensions.width;

        this.strokeWidth = this.getStrokeWidth();

        this.points = this.getPolygon();

        // Not sure if it is the font, but aligning the y position with
        // 50% does not center it. So doing our own calculations here
        // to align the text correctly inside the button.
        this.textPositionY = this.getTextPositionY();
    }

    removeEventListeners() {
        this.polygon.removeEventListener('mouseenter', this.onMouseEnter);
        this.polygon.removeEventListener('mouseleave', this.onMouseLeave);
    }

    disconnectResizeObserver() {
        this.resizeObserver.disconnect();
    }

    getButtonDimensions() {
        const styleDeclaration = getComputedStyle(this.container);

        const computedHeight = styleDeclaration.getPropertyValue('height');
        const computedWidth = styleDeclaration.getPropertyValue('width');

        const height = parseInt(computedHeight);
        const width = parseInt(computedWidth);

        return { width, height };
    }

    getStrokeWidth() {
        const styleDeclaration = getComputedStyle(this.polygon);

        const strokeWidth = styleDeclaration.getPropertyValue('stroke-width');

        return parseInt(strokeWidth);
    }

    getPolygon() {
        // Due to the way the SVG draws we need to offset the polygon lines with
        // half the stroke width otherwise our diagonal lines will appear thicker.
        const inset = this.strokeWidth / 2;

        const cutoffX = 20;
        const cutoffY = 15;

        // The points for the border of the button with two cutoff corners.
        const points = [
            [inset, inset],
            [this.width - cutoffX, inset],
            [this.width - inset, cutoffY],
            [this.width - inset, this.height - inset],
            [cutoffX, this.height - inset],
            [inset, this.height - cutoffY]
        ];

        // Converts to a string with a list of points that we can give to the
        // svg points. Using the default values this 2D array will result
        // in a string like this: 0.5,0.5 180,0.5 200,20 200,31.5 20,31.5 0.5,12 0.5,0.5
        return points.map((point) => point.join(',')).join(' ');
    }

    getTextPositionY() {
        const strokeOffset = this.strokeWidth * 2;
        const position = this.height / 2 + strokeOffset;

        return `${position}px`;
    }
}
