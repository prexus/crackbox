import { Component } from '@angular/core';

import { Task } from '@cbox-api/interfaces/task.interface';
import { TaskService } from './services/task.service';

@Component({
    selector: 'cbox-app',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss']
})
export class AppComponent {
    basePairProgressRows = 35;
    basePairWidth = 80;
    progress: number[];

    showCreateTask = false;
    selectedTask: Task;

    constructor(private taskService: TaskService) {}

    onOpenCreateTask() {
        this.showCreateTask = true;
    }

    onCloseCreateTask() {
        this.showCreateTask = false;
    }

    onSelectTask(task: Task) {
        this.selectedTask = task;

        this.progress = this.taskService.convertChunkProgress(task.progress, this.basePairProgressRows, this.basePairWidth);
    }
}
