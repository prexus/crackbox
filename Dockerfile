# This Dockerfile is used by Gitlab CI to build a docker
# image from the artifact from a previous job. If run locally
# you would need crackbox-latest.tar.gz in the root directory
# for it to build successfully.
FROM node:18-alpine

# Extracts the latest version and install dependencies
ADD crackbox-latest.tar.gz /app

WORKDIR /app

RUN yarn install

# Expose the applications default port. Note that in case
# it is changed by overriding the default using the environment
# variable CRACKBOX_PORT this will be misleading.
EXPOSE 3000

ENTRYPOINT exec node main.js
