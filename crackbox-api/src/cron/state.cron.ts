import { Injectable } from '@nestjs/common';
import { Cron } from '@nestjs/schedule';

import { AgentService } from '@cbox-api/services/agent.service';
import { CronService } from '@cbox-api/services/cron.services';
import { StateService } from '@cbox-api/services/state.service';
import { TaskService } from '@cbox-api/services/task.service';

@Injectable()
export class StateCron {
    constructor(
        private agentService: AgentService,
        private cronService: CronService,
        private taskService: TaskService,
        private stateService: StateService
    ) { }

    @Cron('*/10 * * * * *')
    async agentPolling() {
        this.cronService.run('agentPolling', async () => {
            const agents = await this.agentService.getAgents();

            this.stateService.updateAgents(agents);
        });
    }

    @Cron('*/10 * * * * *')
    taskPolling() {
        this.cronService.run('taskPolling', async () => {
            const tasks = await this.taskService.getTasks();

            this.stateService.updateTasks(tasks);
        });
    }
}
