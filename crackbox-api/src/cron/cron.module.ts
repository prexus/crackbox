import { Module } from '@nestjs/common';

import { ServiceModule } from '@cbox-api/services/service.module';

import { AgentCron } from './agent.cron';
import { StateCron } from './state.cron';
import { TaskCron } from './task.cron';

@Module({
    imports: [
        ServiceModule
    ],
    providers: [
        TaskCron,
        AgentCron,
        StateCron
    ]
})
export class CronModule { }
