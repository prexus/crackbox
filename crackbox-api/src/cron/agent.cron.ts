import { Logger, Injectable } from '@nestjs/common';
import { Cron } from '@nestjs/schedule';

import { AgentService } from '@cbox-api/services/agent.service';

@Injectable()
export class AgentCron {
    private logger = new Logger(AgentCron.name);

    constructor(private agentService: AgentService) {}

    @Cron('*/10 * * * * *')
    async handleAgentMaintenance() {
        const agents = await this.agentService.getAgents();
        const failedAgents = agents.filter((agent) => !agent.active);

        for (const agent of failedAgents) {
            this.logger.log(`Agent #${agent.id} failed, unassigning current task and reactivating the agent`);

            await this.agentService.unassignAgent(agent.id);
            await this.agentService.reactivateAgent(agent.id);
        }
    }
}
