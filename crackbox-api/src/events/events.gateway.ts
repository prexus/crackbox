import { WebSocketGateway } from '@nestjs/websockets';

import { WebSocket } from 'ws';

import { EventService } from '@cbox-api/services/event.service';
import { StateService } from '@cbox-api/services/state.service';

@WebSocketGateway({ path: '/websocket' })
export class EventsGateway {
    constructor(private eventsService: EventService, private stateService: StateService) { }

    handleConnection(client: WebSocket) {
        this.eventsService.addClient(client);

        this.sendInitialStateMessage(client);

        client.on('close', () => {
            this.eventsService.removeClient(client);
        });
    }

    sendInitialStateMessage(client: WebSocket) {
        const data = this.stateService.getFullUpdate();
        const message = JSON.stringify(data);

        client.send(message);
    }
}
