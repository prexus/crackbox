import { Controller, Get } from '@nestjs/common';

import { HashTypeService } from '@cbox-api/services/hash-type.service';

@Controller('hash-types')
export class HashTypesController {
    constructor(private hashTypeService: HashTypeService) {}

    @Get()
    async getHashTypes() {
        return this.hashTypeService.getHashTypes();
    }
}
