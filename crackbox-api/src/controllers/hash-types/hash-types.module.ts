import { Module } from '@nestjs/common';

import { ServiceModule } from '@cbox-api/services/service.module';

import { HashTypesController } from './hash-types.controller';

@Module({
    imports: [ServiceModule],
    controllers: [HashTypesController]
})
export class HashTypesModule { }
