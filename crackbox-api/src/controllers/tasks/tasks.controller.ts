import { Body, Controller, Delete, Get, Header, Param, Post } from '@nestjs/common';

import { TaskService } from '@cbox-api/services/task.service';

import { TaskRecipe } from '@cbox-api/classes/task-recipe';

@Controller('tasks')
export class TasksController {
    constructor(private taskService: TaskService) {}

    @Post()
    async createTask(@Body() task: TaskRecipe) {
        return this.taskService.createTask(task);
    }

    @Delete('/:id')
    async deleteTask(@Param('id') taskId: number) {
        return this.taskService.deleteTask(taskId);
    }

    @Get('/:id/download')
    @Header('Content-Type', 'application/json')
    @Header('Content-Disposition', 'attachment; filename=passwords.txt')
    async getCrackedPasswords(@Param('id') taskId: number) {
        const passwords = await this.taskService.getCrackedPasswords(taskId);

        return passwords.join('\n');
    }
}
