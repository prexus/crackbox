import { join } from 'path';

import { Module } from '@nestjs/common';

import { ConfigModule } from '@nestjs/config';
import { ScheduleModule } from '@nestjs/schedule';
import { ServeStaticModule } from '@nestjs/serve-static';

import { CronModule } from 'cron/cron.module';
import { EventsModule } from 'events/events.module';
import { TasksModule } from 'controllers/tasks/tasks.module';
import { HashTypesModule } from 'controllers/hash-types/hash-types.module';

@Module({
    imports: [
        // NestJS Modules
        ConfigModule.forRoot(),
        ScheduleModule.forRoot(),
        ServeStaticModule.forRoot({
            rootPath: join(__dirname, 'ui'),
            exclude: ['/api*']
        }),

        // Crackbox Modules
        TasksModule,
        HashTypesModule,
        EventsModule,
        CronModule
    ]
})
export class AppModule {}
