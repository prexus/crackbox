import { BadGatewayException, BadRequestException, Injectable, Logger } from '@nestjs/common';

@Injectable()
export class ApiService {
    private logger = new Logger(ApiService.name);

    protected async send<RequestType, ResponseType>(query: RequestType): Promise<ResponseType> {
        const options = {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: this.prepareApiRequest<RequestType>(query)
        };

        try {
            const url = `${process.env.HASHTOPOLIS_URL}/api/user.php`;
            const body = await fetch(url, options);
            const json = await body.json();

            if (json.response !== 'OK') {
                throw new BadRequestException(json.message);
            }

            return json;
        } catch (e) {
            const errorMessage = 'An error occured while contacting the Hashtopolis API';

            this.logger.error(`${errorMessage}: ${JSON.stringify(e)}`);

            throw new BadGatewayException(errorMessage);
        }
    }

    private prepareApiRequest<RequestType>(query: RequestType) {
        const queryWithKey = { ...query, accessKey: process.env.HASHTOPOLIS_API_KEY };
        const queryForBody = JSON.stringify(queryWithKey);

        return queryForBody;
    }
}
