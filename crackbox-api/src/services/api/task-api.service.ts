import { Injectable } from '@nestjs/common';

import { DataService } from '@cbox-api/services/data.service';

import { CreateTaskRequest, CreateTaskResponse } from '@cbox-api/dtos/create-task.dto';
import { DeleteTaskRequest, DeleteTaskResponse } from '@cbox-api/dtos/delete-task.dto.';
import { GetChunkRequest, GetChunkResponse } from '@cbox-api/dtos/get-chunk.dto';
import { GetTaskRequest, GetTaskResponse } from '@cbox-api/dtos/get-task.dto';
import { ListTasksRequest, ListTasksResponse } from '@cbox-api/dtos/list-tasks.dto';
import { SetTaskPriorityRequest, SetTaskPriorityResponse } from '@cbox-api/dtos/set-task-priority.dto';
import { TaskUnassignAgentRequest, TaskUnassignAgentResponse } from '@cbox-api/dtos/task-unassign-agent.dto';

import { ApiService } from './api.service';

@Injectable()
export class TaskApiService extends ApiService {
    constructor(protected dataService: DataService) {
        super();
    }

    listTasks() {
        const query = {
            section: 'task',
            request: 'listTasks'
        };

        return this.send<ListTasksRequest, ListTasksResponse>(query);
    }

    getTask(id: number) {
        const query = {
            section: 'task',
            request: 'getTask',
            taskId: id
        };

        return this.send<GetTaskRequest, GetTaskResponse>(query);
    }

    getChunk(id: number) {
        const query = {
            section: 'task',
            request: 'getChunk',
            chunkId: id
        };

        return this.send<GetChunkRequest, GetChunkResponse>(query);
    }

    createTask(taskName: string, taskHashlistId: number) {
        const { rockYou, oneRuleToRuleThemAll } = this.dataService.getFiles();

        const query = {
            section: 'task',
            request: 'createTask',
            name: taskName,
            hashlistId: taskHashlistId,
            attackCmd: `#HL# ${rockYou.filename} -r ${oneRuleToRuleThemAll.filename}`,
            chunksize: 600,
            statusTimer: 5,
            benchmarkType: 'speed',
            color: '5D5D5D',
            isCpuOnly: true,
            isSmall: false,
            skip: 0,
            crackerVersionId: 1,
            files: [
                rockYou.id,
                oneRuleToRuleThemAll.id
            ],
            priority: 100,
            preprocessorId: 0,
            preprocessorCommand: ''
        };

        return this.send<CreateTaskRequest, CreateTaskResponse>(query);
    }

    deleteTask(id: number) {
        const query = {
            section: 'task',
            request: 'deleteTask',
            taskId: id
        };

        return this.send<DeleteTaskRequest, DeleteTaskResponse>(query);
    }

    setTaskPriority(taskId: number, priority: number) {
        const query = {
            section: 'task',
            request: 'setTaskPriority',
            taskId: taskId,
            priority: priority
        };

        return this.send<SetTaskPriorityRequest, SetTaskPriorityResponse>(query);
    }

    taskUnassignAgent(agentId: number) {
        const query = {
            section: 'task',
            request: 'taskUnassignAgent',
            agentId: agentId
        };

        return this.send<TaskUnassignAgentRequest, TaskUnassignAgentResponse>(query);
    }
}
