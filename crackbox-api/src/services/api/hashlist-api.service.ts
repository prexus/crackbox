import { Injectable } from '@nestjs/common';

import { CreateHashlistRequest, CreateHashlistResponse } from '@cbox-api/dtos/create-hashlist.dto';
import { DeleteHashlistRequest, DeleteHashlistResponse } from '@cbox-api/dtos/delete-hashlist.dto';
import { GetCrackedRequest, GetCrackedResponse } from '@cbox-api/dtos/get-cracked.dto';
import { GetHashlistRequest, GetHashlistResponse } from '@cbox-api/dtos/get-hashlist.dto';

import { ApiService } from './api.service';

@Injectable()
export class HashlistApiService extends ApiService {
    createHashlist(hashlistName: string, base64data: string, hashType: number) {
        const query = {
            section: 'hashlist',
            request: 'createHashlist',
            name: hashlistName,
            isSalted: false,
            isSecret: false,
            isHexSalt: false,
            separator: ':',
            format: 0,
            hashtypeId: hashType,
            accessGroupId: 1,
            data: base64data,
            useBrain: false,
            brainFeatures: 0
        };

        return this.send<CreateHashlistRequest, CreateHashlistResponse>(query);
    }

    getHashlist(id: number) {
        const query = {
            section: 'hashlist',
            request: 'getHashlist',
            hashlistId: id
        };

        return this.send<GetHashlistRequest, GetHashlistResponse>(query);
    }

    deleteHashlist(id: number) {
        const query = {
            section: 'hashlist',
            request: 'deleteHashlist',
            hashlistId: id
        };

        return this.send<DeleteHashlistRequest, DeleteHashlistResponse>(query);
    }

    getCracked(hashlistId: number) {
        const query = {
            section: 'hashlist',
            request: 'getCracked',
            hashlistId: hashlistId
        };

        return this.send<GetCrackedRequest, GetCrackedResponse>(query);
    }
}
