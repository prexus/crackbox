import { Injectable } from '@nestjs/common';

import { Agent } from '@cbox-api/interfaces/agent.interface';

import { ChunkStatus } from '@cbox-api/enums/chunk-status.enum';
import { BlockStatus } from '@cbox-api/enums/block-status.enum';

import { GetTaskResponse } from '@cbox-api/dtos/get-task.dto';
import { GetChunkResponse } from '@cbox-api/dtos/get-chunk.dto';

import { TaskApiService } from './api/task-api.service';

@Injectable()
export class ChunkService {
    constructor(
        private taskApiService: TaskApiService
    ) {}

    async getChunkProgress(details: GetTaskResponse) {
        const chunkResponse = await this.getTaskChunks(details.chunkIds);

        const progress = chunkResponse.map((chunk) => chunk.progress);

        const unassignedChunks = this.getUnassignedChunkEstimate(details);
        const unassignedProgress = new Array(unassignedChunks).fill(0);

        progress.push(...unassignedProgress);

        return progress;
    }

    async getTaskChunks(chunkIds: number[]) {
        const chunkPromises = chunkIds.map((id) => this.taskApiService.getChunk(id));
        const chunks: GetChunkResponse[] = await Promise.all(chunkPromises);

        return chunks.sort(this.sortChunksByKeyspaceStart);
    }

    sortChunksByKeyspaceStart(chunk1: GetChunkResponse, chunk2: GetChunkResponse) {
        return (chunk1.start > chunk2.start) ? 1 : -1;
    }

    getUnassignedChunkEstimate(details: GetTaskResponse) {
        const currentChunkCount = details.chunkIds.length;

        if (currentChunkCount === 0) {
            return 0;
        }

        const currentChunkLength = details.dispatched;
        const averageChunkLength = currentChunkLength / currentChunkCount;
        const remainingKeyspace = details.keyspace - details.dispatched;
        const unassignedChunks = Math.ceil(remainingKeyspace / averageChunkLength);

        return unassignedChunks;
    }

    getTaskChunkStatus(chunk: GetChunkResponse) {
        if (chunk.state >= 6) {
            return ChunkStatus.FAILED;
        }

        if (chunk.progress === 100) {
            return ChunkStatus.COMPLETED;
        }

        if (chunk.progress > 0) {
            return ChunkStatus.RUNNING;
        }

        return ChunkStatus.PENDING;
    }

    getTaskChunkBlocks(chunk?: GetChunkResponse) {
        const blocks = [];
        const blocksPerChunk = 18;
        const completedBlocks = chunk ? Math.floor(blocksPerChunk * (chunk.progress / 100)) : 0;

        for (let block = 1; block <= blocksPerChunk; block++) {
            if (!chunk) {
                blocks.push(BlockStatus.PENDING);
            } else if (block <= completedBlocks) {
                blocks.push(BlockStatus.COMPLETED);
            } else if (block === completedBlocks + 1) {
                blocks.push(BlockStatus.RUNNING);
            } else {
                blocks.push(BlockStatus.PENDING);
            }
        }

        return blocks;
    }

    getAgentName(chunkResponse: GetChunkResponse, agents: Agent[]) {
        const agentId = chunkResponse.agentId;

        const assignedAgent = agents.find((agent) => agent.id === agentId);

        return assignedAgent?.name ?? null;
    }
}
