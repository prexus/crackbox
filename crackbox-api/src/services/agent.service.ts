import { Injectable } from '@nestjs/common';

import { Agent } from '@cbox-api/interfaces/agent.interface';

import { TaskApiService } from './api/task-api.service';
import { AgentApiService } from './api/agent-api.service';

@Injectable()
export class AgentService {
    constructor(
        private taskApiService: TaskApiService,
        private agentApiService: AgentApiService
    ) {}

    async getAgents() {
        const agentsList = await this.agentApiService.listAgents();

        const agents: Agent[] = agentsList.agents.map((agent) => ({
            id: parseInt(agent.agentId),
            name: agent.name,
            devices: agent.devices,
            active: false,
            lastActivity: 0
        }));

        for (const agent of agents) {
            const details = await this.agentApiService.getAgent(agent.id);

            agent.active = details.isActive;
            agent.lastActivity = details.lastActivity.time;
        }

        return agents;
    }

    async reactivateAgent(agentId: number) {
        await this.agentApiService.setActive(agentId, true);
    }

    async unassignAgent(agentId: number) {
        await this.taskApiService.taskUnassignAgent(agentId);
    }
}
