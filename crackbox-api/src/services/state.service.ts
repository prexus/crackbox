import { Injectable } from '@nestjs/common';

import isEqual from 'lodash.isequal';

import { Agent } from '@cbox-api/interfaces/agent.interface';
import { Task } from '@cbox-api/interfaces/task.interface';

import { EventService } from './event.service';

@Injectable()
export class StateService {
    agents: Agent[] = [];
    tasks: Task[] = [];

    constructor(private eventService: EventService) {}

    updateAgents(agents: Agent[]) {
        if (isEqual(this.agents, agents)) {
            return;
        }

        this.agents = agents;

        this.eventService.broadcast({ agents });
    }

    updateTasks(tasks: Task[]) {
        if (isEqual(this.tasks, tasks)) {
            return;
        }

        this.tasks = tasks;

        this.eventService.broadcast({ tasks });
    }

    getFullUpdate() {
        return {
            agents: this.agents,
            tasks: this.tasks
        };
    }
}
