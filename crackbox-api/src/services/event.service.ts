import { Injectable } from '@nestjs/common';

import { WebSocket } from 'ws';

import { Message } from '@cbox-api/interfaces/message.interface';

@Injectable()
export class EventService {
    clients: WebSocket[] = [];

    addClient(client: WebSocket) {
        this.clients.push(client);
    }

    removeClient(client: WebSocket) {
        const index = this.clients.indexOf(client);

        this.clients.splice(index, 1);
    }

    broadcast(message: Message) {
        const payload = JSON.stringify(message);

        this.clients.forEach((client) => client.send(payload));
    }
}
