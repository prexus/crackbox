import { Injectable, Logger } from '@nestjs/common';

@Injectable()
export class CronService {
    private logger = new Logger(CronService.name);

    private jobStatus = {};

    // Runs the provided jobFn only if it was completed from its previous run
    async run(name: string, jobFn: Function) {
        if (this.isRunning(name)) {
            return;
        }

        this.markRunning(name);

        try {
            await jobFn();
        } catch (e) {
            this.logger.error(`Cron job ${name} failed with: ${e.message}`);
        } finally {
            this.markDone(name);
        }
    }

    private markRunning(job: string) {
        this.jobStatus[job] = true;
    }

    private markDone(job: string) {
        this.jobStatus[job] = false;
    }

    private isRunning(job: string) {
        return this.jobStatus[job];
    }
}
