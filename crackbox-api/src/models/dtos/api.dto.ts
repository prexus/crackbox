export interface ApiRequest {
    section: string;
    request: string;
    accessKey?: string;
}

export interface ApiResponse {
    section: string;
    request: string;
    response: string;
}
