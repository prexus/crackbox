import { ApiRequest, ApiResponse } from './api.dto';

export interface ListTasksRequest extends ApiRequest {}

export interface ListTasksResponse extends ApiResponse {
    tasks: ListTasksTaskEntry[]
}

export interface ListTasksTaskEntry {
    taskId?: number;
    supertaskId?: number;
    name: string;
    type: number;
    hashlistId: number;
    priority: number;
}
