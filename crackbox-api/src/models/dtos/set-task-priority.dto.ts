import { ApiRequest, ApiResponse } from './api.dto';

export interface SetTaskPriorityRequest extends ApiRequest {
    taskId: number;
    priority: number;
}

export interface SetTaskPriorityResponse extends ApiResponse { }
