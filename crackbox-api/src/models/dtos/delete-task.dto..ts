import { ApiRequest, ApiResponse } from './api.dto';

export interface DeleteTaskRequest extends ApiRequest {
    taskId: number;
}

export interface DeleteTaskResponse extends ApiResponse { }
