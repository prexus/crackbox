import { ApiRequest, ApiResponse } from './api.dto';

export interface TaskUnassignAgentRequest extends ApiRequest {
    agentId: number;
}

export interface TaskUnassignAgentResponse extends ApiResponse { }
