/* eslint-disable @typescript-eslint/indent */

import { IsBoolean, IsNumber } from 'class-validator';

export class TaskPatch {
    @IsNumber()
        taskId: number;

    @IsBoolean()
        active: boolean;
}
