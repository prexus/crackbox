/* eslint-disable @typescript-eslint/indent */

import { IsString, MinLength, IsBase64 } from 'class-validator';

import { IsHashTypeId } from '@cbox-api/pipes/hash-types.pipe';

export class TaskRecipe {
    @IsString()
    @MinLength(3)
    name: string;

    @IsHashTypeId()
    hashTypeId: number;

    @IsBase64()
    data: string;
}
