export interface Agent {
    id: number;
    name: string;
    devices: string[];
    active: boolean;
    lastActivity: number;
}
