import { Agent } from './agent.interface';
import { Task } from './task.interface';

export interface Message {
    agents?: Agent[];
    tasks?: Task[];
}
