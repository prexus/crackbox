import { TaskStatus } from '@cbox-api/enums/task-status.enum';

export interface Task {
    id: number;
    name: string;
    hashType: string;
    status: TaskStatus;
    percentage: number;
    progress: number[];
    speed: number;
    agents: number;
    hashes: number;
    cracked: number;
}
