export enum BlockStatus {
    PENDING,
    RUNNING,
    COMPLETED
}
