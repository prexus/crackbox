# Crackbox Deployment
This deployment project uses Ansible to target 4x Raspberry Pi 4 running the official [Ubuntu 21.10 - Raspberry Pi Generic (64-bit ARM) preinstalled server image](https://cdimage.ubuntu.com/releases/21.10/release/).

You can find more details on the design decisions made in this project on the [Crackbox Deployment wiki page](https://gitlab.com/prexus/crackbox/-/wikis/crackbox-deployment).

## Running the playbook

To run the entire playbook, use the following command:

```bash
$ ansible-playbook site.yml --user <user> -kK
```

You can configure your inventory-file with credentials to skip that step.

## Targeting a group
To only target the master or worker nodes you can run the playbook with a target group specified:

```bash
$ ansible-playbook -l master site.yml -Kku <user>
$ ansible-playbook -l nodes site.yml -Kku <user>
```

## Turning off the cluster
In case you need to move the cluster, a quick way of turning off all the nodes are running the shutdown command using ansible:

```bash
$ ansible all -a "sudo shutdown now" -Kku <user>
```

## An important note
The default password for the Hashtopolis installation is admin/admin and should be changed after installation. You can Hashtopolis on port :8080 on your master node (eg. http://crackbox1:8080 in this deployment)

