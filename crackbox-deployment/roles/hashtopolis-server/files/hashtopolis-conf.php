<?php

//START CONFIG
$CONN['user'] = 'hashtopolis';
$CONN['pass'] = 'hashtopolis';
$CONN['server'] = 'localhost';
$CONN['db'] = 'hashtopolis';
$CONN['port'] = '3306';

$PEPPER = [
  "qT0mt8YkLz7Xa4ZVguTpYT7KswvDP3kuvUkF5coZdPUCuN2SQx",
  "nqsyARaXuKQv2UOPCOiLsk09M4o3u6olhXiFcEM5gflajKjaNB",
  "nCbmoJgF2abnf5ezDIZOw2sXeFuH9b7ltSkgZv5GoMLiF8hHrO",
  "E0iKcw6muXgF6np7wu84s5BQbGRpKnLN8nL4LzVX"
];

$INSTALL = true; //set this to true if you config the mysql and setup manually

//END CONFIG
