# Crackbox
<div align="center">
  <img alt="Pipelines" src="https://gitlab.com/prexus/crackbox/badges/master/pipeline.svg" />
  <img alt="Latest release" src="https://gitlab.com/prexus/crackbox/-/badges/release.svg" />
</div>

## What is Crackbox

Crackbox is the name of my little Raspberry Pi cluster that evolved into a small distributed password cracking utility. It is a proof of concept for a simple interface to distributed password cracking building on top of existing open source projects.

<div align="center">
    <br>
    <img src="https://gitlab.com/prexus/crackbox/-/wikis/assets/raspberry-pi-cluster.jpg" alt="Raspberry Pi Cluster">
    &nbsp;&nbsp;&nbsp;
    <a href="https://gitlab.com/prexus/crackbox/-/wikis/assets/crackbox-ui-desktop.png" title="Crackbox UI (Desktop)">
        <img src="https://gitlab.com/prexus/crackbox/-/wikis/assets/crackbox-ui-desktop-small.png" alt="Crackbox UI (Desktop)" height="225"
    </a>
  <br>
  <br>
</div>

Please refer to the [wiki](https://gitlab.com/prexus/crackbox/-/wikis/home) for further details about this project.

Individual project details follows below.

## Releases
Find the releases [here](https://gitlab.com/prexus/crackbox/-/releases)

## Projects
The mono repo covers four projects that each has played its own part in the making of the Crackbox. Each project is has a README.md explaining how the project is used. All the details outlining things like design decisions, challenges and improvement suggestions are available on their respective wiki pages.

### Project details

<table width="100%">
<tr>
<td width="50%">

  #### Crackbox API
  An intermediate NestJS API that simplify the complexity of Hashtopolis. Crackbox API offers HTTP endpoints, WebSockets and Swagger documentation.

  ##### Project documentation
  - [crackbox-api/README.md](./crackbox-api/README.md) for instructions
  - [Crackbox API Wiki](https://gitlab.com/prexus/crackbox/-/wikis/crackbox-api) for project details

  </td>
  <td>

  #### Crackbox UI
  An Angular SPA communicating with the Crackbox API over HTTP and WebSockets to create and display tasks and agent status from Hashtopolis.

  ##### Project documentation
  - [crackbox-ui/README.md](./crackbox-ui/README.md) for instructions
  - [Crackbox UI Wiki](https://gitlab.com/prexus/crackbox/-/wikis/crackbox-ui) for project details

</td>
</tr>
<tr>
<td>

  #### Crackbox Deployment
  Ansible playbook for deploying the full Crackbox onto four Raspberry Pi with one running as master node and the remaining three as workers.

  ##### Project documentation
  - [crackbox-deployment/README.md](./crackbox-deployment/README.md) for instructions
  - [Crackbox Deployment Wiki](https://gitlab.com/prexus/crackbox/-/wikis/crackbox-deployment) for project details

</td>
<td>

  #### Hashtopolis Mock API
  A simple Node.js API made with Express that mimics the response of a Hashtopolis installation that can be used for testing during development.

  ##### Project documentation
  - [hashtopolis-mock-api/README.md](./hashtopolis-mock-api/README.md) for instructions
  - [Hashtopolis Mock API Wiki](https://gitlab.com/prexus/crackbox/-/wikis/hashtopolis-mock-api) for project details

</td>
</tr>
</table>
