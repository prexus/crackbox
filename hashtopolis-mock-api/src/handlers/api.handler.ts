import { Request, Response } from 'express';

import { mockService } from '@services/mock.service';

import { RequestBody } from '@interfaces/request-body';

const handler = {
    user
};

function user(req: Request, res: Response) {
    const query = req.body as RequestBody;
    const section = mockService.getSection(query.section);

    if (!section) {
        res.statusCode = 404;
        res.send(`Invalid section (${query.section || 'missing'})`);

        return;
    }

    const mockedResponse = section[query.request](query);

    res.send(mockedResponse);
}

export const apiHandler = handler;
