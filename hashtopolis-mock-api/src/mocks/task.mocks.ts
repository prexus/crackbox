const mocks = {
    createTask,
    listTasks,
    getTask,
    deleteTask,
    setTaskPriority,
    taskUnassignAgent,
    getChunk
};

function createTask() {
    return {
        section: 'task',
        request: 'createTask',
        response: 'OK',
        taskId: 101
    };
}

function listTasks() {
    return {
        section: 'task',
        request: 'listTasks',
        response: 'OK',
        tasks: [
            {
                taskId: 7587,
                name: 'Mem Dump',
                type: 0,
                hashlistId: 1,
                priority: 5
            },
            {
                supertaskId: 33,
                name: 'Harddrive recovery #921',
                type: 1,
                hashlistId: 1,
                priority: 3
            },
            {
                supertaskId: 32,
                name: 'WinZip Password Recovery',
                type: 1,
                hashlistId: 1,
                priority: 0
            },
            {
                taskId: 7580,
                name: 'Bitwarden Extract',
                type: 0,
                hashlistId: 1,
                priority: 0
            }
        ]
    };
}

function getTask() {
    return {
        section: 'task',
        request: 'getTask',
        response: 'OK',
        taskId: 7587,
        name: 'testing',
        attack: '#HL# -a 0 top10000.txt -r dive.rule',
        chunksize: 600,
        color: null,
        benchmarkType: 'speed',
        statusTimer: 5,
        priority: 0,
        isCpuOnly: false,
        isSmall: false,
        skipKeyspace: 0,
        keyspace: 100000,
        dispatched: 30000,
        hashlistId: 1,
        imageUrl: 'http://localhost/hashtopolis/src/api/taskimg.php?task=7587',
        files: [
            {
                fileId: 2,
                filename: 'dive.rule',
                size: 887155
            },
            {
                fileId: 3653,
                filename: 'top10000.txt',
                size: 76508
            }
        ],
        speed: 30000000,
        searched: 30000,
        chunkIds: [
            31,
            32,
            33
        ],
        agents: [
            {
                agentId: 1,
                benchmark: '0',
                speed: 10000000
            },
            {
                agentId: 2,
                benchmark: '0',
                speed: 10000000
            },
            {
                agentId: 3,
                benchmark: '0',
                speed: 10000000
            }
        ],
        isComplete: false,
        usePreprocessor: false,
        preprocessorId: 0,
        preprocessorCommand: ''
    };
}

function deleteTask() {
    return {
        section: 'task',
        request: 'deleteTask',
        response: 'OK'
    };
}

function setTaskPriority() {
    return {
        section: 'task',
        request: 'setTaskPriority',
        response: 'OK'
    };
}

function taskUnassignAgent() {
    return {
        section: 'task',
        request: 'taskUnassignAgent',
        response: 'OK'
    };
}

function getChunk() {
    return {
        section: 'task',
        request: 'getChunk',
        response: 'OK',
        chunkId: 30,
        start: 23141360,
        length: 5785340,
        checkpoint: 28926700,
        progress: 80,
        taskId: 7585,
        agentId: 2,
        dispatchTime: 1531313146,
        lastActivity: 1531313738,
        state: 4,
        cracked: 0,
        speed: 10000000
    };
}

export const taskMocks = mocks;
