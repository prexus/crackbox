import { RequestBody } from '@interfaces/request-body';

const mocks = {
    connection,
    access
};

function connection() {
    return {
        section: 'test',
        request: 'connection',
        response: 'SUCCESS'
    };
}

function access(options: RequestBody) {
    const key = options.accessKey || 'INVALID';

    if (key !== 'INVALID') {
        return {
            section: 'test',
            request: 'access',
            response: 'OK'
        };
    }

    return {
        section: 'test',
        request: 'access',
        response: 'ERROR',
        message: 'API key was not found!'
    };
}

export const testMocks = mocks;
