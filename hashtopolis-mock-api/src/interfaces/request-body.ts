export interface RequestBody {
    section: string,
    request: string,
    accessKey?: string
}
