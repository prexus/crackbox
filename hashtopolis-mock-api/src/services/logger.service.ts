/* eslint-disable no-console */

import { Request, Response, NextFunction } from 'express';

const service = {
    logWelcome,
    logRequest
};

function logWelcome(port: number) {
    console.log('  ___ ___               .__     __                       .__  .__        ');
    console.log(' /   |   \\_____    _____|  |___/  |_  ____ ______   ____ |  | |__| ______');
    console.log('/    ~    \\__  \\  /  ___/  |  \\   __\\/  _ \\\\____ \\ /  _ \\|  | |  |/  ___/');
    console.log('\\    Y    // __ \\_\\___ \\|   Y  \\  | (  <_> )  |_> >  <_> )  |_|  |\\___ \\ ');
    console.log(' \\___|_  /(____  /____  >___|  /__|  \\____/|   __/ \\____/|____/__/____  >');
    console.log('       \\/      \\/     \\/     \\/            |__|              Mock API \\/ ');
    console.log();
    console.log('Available on port', port);
    console.log();
}

function logRequest(req: Request, res: Response, next: NextFunction) {
    console.log();
    console.log(new Date());
    console.log(` Request:  ${req.body.section} -> ${req.body.request}`);

    const oldWrite = res.write;
    const oldEnd = res.end;

    const chunks = [];

    res.write = (...args) => {
        chunks.push(Buffer.from(args[0]));

        return oldWrite.apply(res, args);
    };

    res.end = (...args) => {
        if (args[0]) {
            chunks.push(Buffer.from(args[0]));
        }

        const responseBody = Buffer.concat(chunks).toString('utf8');
        const responseLog = getResponseLogString(responseBody);

        console.log(` Response: ${responseLog}`);

        return oldEnd.apply(res, args);
    };

    next();
}

function getResponseLogString(value: string) {
    try {
        const json = JSON.parse(value);

        return json.response;
    } catch (e) {
        return '[invalid json]';
    }
}

export const loggerService = service;
