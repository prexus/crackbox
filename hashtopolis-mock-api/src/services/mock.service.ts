import { testMocks } from '@mocks/test.mocks';
import { taskMocks } from '@mocks/task.mocks';
import { hashlistMocks } from '@mocks/hashlist.mocks';
import { agentMocks } from '@mocks/agent.mocks';

const service = {
    getSection
};

const sections = {
    hashlist: hashlistMocks,
    test: testMocks,
    task: taskMocks,
    agent: agentMocks
};

function getSection(section: string) {
    return sections[section];
}

export const mockService = service;
