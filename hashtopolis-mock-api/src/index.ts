import express from 'express';
import bodyParser from 'body-parser';

import { apiHandler } from '@handlers/api.handler';
import { loggerService } from '@services/logger.service';

const server = express();

server.use(bodyParser.json());
server.use(loggerService.logRequest);

const port = 3100;

server.post('/api/user.php', apiHandler.user);

server.listen(port, () => loggerService.logWelcome(port));
